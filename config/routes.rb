Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :login, only: [:create]
      resources :recipients, only: [:index, :create]
      match 'recipients/:name' => 'recipients#show', via: [:get]
      resources :payments, only: [:index, :create]
    end
  end
end
