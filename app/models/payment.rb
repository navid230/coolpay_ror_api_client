require 'rubygems'
require 'active_resource'

class Payment < ActiveResource::Base
  self.include_format_in_path = false
  self.site = "https://coolpay.herokuapp.com/api"
  self.headers['Content-Type'] = 'application/json'
  self.element_name = ""
  self.prefix = "https://coolpay.herokuapp.com/api"

  def self.payments_list(token)
    assign_token(token)
    Payment.get(:payments, self.headers)
  end

  def self.payment_insertion(token, params)
    assign_token(token)
    Payment.post(:payments, nil, { payment: {amount: params[:amount].to_f, currency: params[:currency] , recipient_id: params[:recipient_id], status: params[:status]} }.to_json)
  end

  def self.assign_token(token)
    self.headers['Authorization'] = "Bearer #{token}"
  end
end
