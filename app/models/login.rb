require 'rubygems'
require 'active_resource'

class Login < ActiveResource::Base
  validates_presence_of :username
  validates_presence_of :apikey

  self.include_format_in_path = false
  self.site = "https://coolpay.herokuapp.com/api/login"
  self.headers['Content-Type'] = 'application/json'
  self.element_name = ""


  def self.login(params)
    Login.post(nil, nil, {username: params[:username], apikey: params[:apikey]}.to_json)
  end
end
