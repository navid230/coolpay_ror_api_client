require 'rubygems'
require 'active_resource'

class Recipient < ActiveResource::Base
    
  self.include_format_in_path = false
  self.site = "https://coolpay.herokuapp.com/api"
  self.headers['Content-Type'] = 'application/json'
  self.element_name = ""
  self.prefix = "https://coolpay.herokuapp.com/api"

  def self.recipients_list(token)
    assign_token(token)
    Recipient.get(:recipients, self.headers)
  end

  def self.recipient_search(token, param)
    assign_token(token)
    self.headers['name'] = "#{param[:name]}"
    Recipient.get(:recipients, self.headers)
  end

  def self.recipient_insertion(token, param)
    assign_token(token)
    Recipient.post(:recipients, nil, { recipient: {name: param[:name]} }.to_json)
  end

  def self.assign_token(token)
    self.headers['Authorization'] = "Bearer #{token}"
  end

end
