class Api::V1::RecipientsController < ApplicationController
  skip_before_action :verify_authenticity_token
  respond_to :json
  def create
    if session[:coolpay_token] != nil
      begin
        recipient = Recipient.recipient_insertion(session[:coolpay_token],recipient_param)
      rescue ActiveResource::ResourceNotFound
        recipients = nil
      end
      if recipient != nil
        render json: recipient.response.body, status: recipient.response.code
      else
        render json: {message: 'failed to insert recipient'}, status: 400
      end
    else
        render json: {message: 'please login first using the following url: http://localhost:3000/api/v1/logins'}, status: 400
    end
  end

  def show
    if session[:coolpay_token] != nil
      begin
        recipients = Recipient.recipient_search(session[:coolpay_token],recipient_param)
      rescue ActiveResource::ResourceNotFound
        recipients = nil
      end
      if recipients != nil
        render json: {recipients: recipients}.to_json, status: 200
      else
        render json: {message: 'No Recipient Found'}, status: 400
      end
    else
        render json: {message: 'please login first using the following url: http://localhost:3000/api/v1/logins'}, status: 400
    end
  end

  def index
    if session[:coolpay_token] != nil
      begin
        recipients = Recipient.recipients_list(session[:coolpay_token])
      rescue ActiveResource::ResourceNotFound
        recipients = nil
      end
      if recipients != nil
        render json: {recipients: recipients}.to_json, status: 200
      else
        render json: {message: 'No Recipients Found'}, status: 400
      end
    else
        render json: {message: 'please login first using the following url: http://localhost:3000/api/v1/logins'}, status: 400
    end
  end
  
  private
    def recipient_param
      params.permit(:name)
    end
end
