class Api::V1::PaymentsController < ApplicationController
  skip_before_action :verify_authenticity_token
  respond_to :json

  def create
    if session[:coolpay_token] != nil
      begin
        payment = Payment.payment_insertion(session[:coolpay_token], payment_params)
      rescue ActiveResource::ResourceNotFound
        payment = nil
      end
      if payment != nil
        render json: payment.response.body, status: payment.response.code
      else
        render json: {message: 'failed to insert payment'}, status: 400
      end
    else
        render json: {message: 'please login first using the following url: http://localhost:3000/api/v1/login'}, status: 400
    end
  end

  def index
    if session[:coolpay_token] != nil
      begin
        payments = Payment.payments_list(session[:coolpay_token])
      rescue ActiveResource::ResourceNotFound
        payments = nil
      end
      if payments != nil
        render json: {payments: payments}.to_json, status: 200
      else
        render json: {message: 'No Payments Found'}, status: 400
      end
    else
        render json: {message: 'please login first using the following url: http://localhost:3000/api/v1/login'}, status: 400
    end
  end

  private
    def payment_params
      params.permit(:amount, :currency, :recipient_id, :status)
    end
end
