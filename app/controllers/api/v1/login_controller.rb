class Api::V1::LoginController < ApplicationController
  skip_before_action :verify_authenticity_token
  respond_to :json
  
  def create
    begin
      login = Login.login(login_params)
      token =  JSON.parse(login.response.body)["token"]
    rescue ActiveResource::ResourceNotFound
      token = nil
    end

    if token != nil
      session[:coolpay_token] = token
      render json: login.response.body, status: login.response.code
    else
      render json: {message: 'incorrect username or apikey'}, status: 400
    end
  end

  private
    def login_params
      params.permit(:username, :apikey)
    end

end
