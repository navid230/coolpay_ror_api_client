# README #

This simple app is an attempt to implement my initial php solution for the coolpay api client, in Ruby on Rails.

The app carries out the following functionalities:

* Authenticate a user to the Coolpay API.
* Add recipients.
* list all the recipients of the authenticated user.
* make payments to specific recipients.
* list all the payments made by the authenticated user.

Further description/documentation of the Coolpay API can be found here:

* http://docs.coolpayapi.apiary.io/

# Installation #

1.Make sure you have the following ruby and rails framework versions (here is a guide for installing Ruby on Rails along with other useful components: https://gorails.com/setup/ubuntu/16.04):

* ruby 2.3.3p222
* rails 4.2.7.1

2.Clone/download the project/repository and then navigate to the directory which the repo/project was downloaded into:

3.Execute the following command to install all the required gems:
```
bundle install

```
4.After installing all the gems, then simply start the rails server locally by executing the following command within the project directory:
```
rails s

```

# Usage #

Use the following paths within Postman extension for easy of accessibility:

* 1.Authenticate a user to the Coolpay API.
```
Post: localhost:3000/api/v1/login
parameters/Request Body:
  {
     username: <username value>
     apikey: <api Key value>
  }
```
* 2.Add recipients.
```
Post: localhost:3000/api/v1/recipients
parameters/Request Body:
  {
     name: <Recipient Name>
  }
```
* 3.List all the recipients of the authenticated user.
```
Get: localhost:3000/api/v1/recipients
parameters/Request Body (Optional):
  {
     name: <Recipient Name>
  }
```
* 4.Make payments to specific recipients.
```
Post: localhost:3000/api/v1/payments
parameters/Request Body:
  {  
     "amount": 10.50,
     "currency": "GBP",
     "recipient_id": "6e7b146e-5957-11e6-8b77-86f30ca893d3"
  }
```
* 5.List all the payments made by the authenticated user.
```
Get: localhost:3000/api/v1/payments

```

# Repo owner or admin #
 Navid Amami